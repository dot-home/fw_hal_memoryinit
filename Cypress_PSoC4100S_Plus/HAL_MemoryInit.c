//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       HAL_RTC.c
\brief      Real time clock handling on hardware base

***********************************************************************************/
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS

/********************************* includes **********************************/
#include "HAL_MemoryInit.h"
#include "stddef.h"
#include "cytypes.h"
#include "CyLib.h"

/********************************* Defines **********************************/
typedef unsigned char __cy_byte_align8 __attribute((aligned(8)));

typedef struct
{
    __cy_byte_align8* pucInit;      //Initial contents of this region
    __cy_byte_align8* pucData;      //Start address of this region
    size_t nInit_Size;              //Size of initial data
    size_t nZero_Size;              //Additional size to be zeroed
}__cy_region;


extern const __cy_region __cy_regions[];
extern const char __cy_region_num __attribute__((weak));
#define __cy_region_num ( (size_t)&__cy_region_num )

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/
extern void __libc_init_array(void);


/*********************************  functions *********************************/
//********************************************************************************
/*!
\author  Kraemer E
\date    13.04.2021
\brief   Initialize memory of the cy-regions
\param   none
\return  none
***********************************************************************************/
void HAL_MemoryInit_Init(void)
{
    // Start with standard memory initialization

    unsigned regions = __cy_region_num;
    const __cy_region* psRegion = __cy_regions;

    // Initialize memory
    for (regions = __cy_region_num; regions != 0u; regions--)
    {
        u32* ulSource = (u32*)psRegion->pucInit;
        u32* ulDest = (u32*)psRegion->pucData;
        unsigned int uiLimit = psRegion->nInit_Size;
        unsigned int uiCount;

        for(uiCount = 0u; uiCount != uiLimit; uiCount += sizeof(u32))
        {
            *ulDest = *ulSource;
            ulDest++;
            ulSource++;
        }
        
        uiLimit = psRegion->nZero_Size;
        for (uiCount = 0u; uiCount != uiLimit; uiCount += sizeof(u32))
        {
            *ulDest = 0u;
            ulDest++;
        }

        psRegion++;
    }

    // Invoke static objects constructors
    __libc_init_array();
}

#endif //PSOC_4100_PLUS
