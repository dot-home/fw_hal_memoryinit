//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       HAL_MemoryInit.h
\brief      Standard memory initialization after startup

***********************************************************************************/
#ifndef _HAL_MEMORYINIT_H_
#define _HAL_MEMORYINIT_H_

#ifdef __cplusplus
extern "C"
{
#endif

/********************************* includes **********************************/
#include "BaseTypes.h"

/***************************** defines / macros ******************************/

/************************ externally visible functions ***********************/
void HAL_MemoryInit_Init(void);


#ifdef __cplusplus
}
#endif

#endif // _HAL_MEMORYINIT_H_

/* [] END OF FILE */
